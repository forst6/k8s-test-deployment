# K8s Test Deployment

Deploys a nginx server that return a simple message.

Includes a Service and a IngressRoute for Treafik.

```
kubectl apply -f namespace.yaml
kubectl apply -f .
```
